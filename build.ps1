# Version of the package we are generating
$VERSION = "2025.01.1"

# Sets the URL where the CI build will upload the generated archive
# Only needs to be updated if `.gitlab-ci.yml` is changed
$CI_URL = "https://gitlab.com/umb-svl/turing/-/jobs/artifacts/main/raw/turing.zip?job=win"

# Scoop `coq` package is defined in the following URL:
$SCOOP_COQ_URL = "https://github.com/rivy/scoop.bucket.scoop-main/raw/refs/heads/master/coq.json"

# The following variable sets `coq` package dependency version
# If value is $null, then script fetches version from $SCOOP_COQ_URL
# Example: "2024.10.0"
$SCOOP_COQ_DEP = $null

# End of user-controlled global variables to set.
###############################################################################

# Fetch $SCOOP_COQ_VERSION remotely
Write-Host "Downloading Scoop's `coq`: $SCOOP_COQ_URL"
$scoop_coq = (Invoke-WebRequest -Uri $SCOOP_COQ_URL).Content
try {
  $scoop_coq = $scoop_coq | ConvertFrom-Json
} catch {
    Write-Host $_.Exception.Message
    Write-Host "Response: $scoop_coq"
    exit 1
}

# Set our package's dependency version:
if ($SCOOP_COQ_DEP -eq $null) {
  $SCOOP_COQ_DEP = $scoop_coq.version
  Write-Host "SCOOP_COQ_DEP is unset, so using scoop's coq version: $SCOOP_COQ_DEP"
}

# Next, we double check that the installer is the correct one
# Installer URL is available at: architecture/64bit/url
try {
  $coq_installer_url = $scoop_coq.architecture.'64bit'.url
  Write-Host "Coq's installer URL: $coq_installer_url"
  if ($coq_installer_url -eq $null) {
    Write-Host "Scoop's `coq` package has unexpected format: architecture/64bit/url is null: $($scoop_coq.architecture.'64bit')"
    exit 1
  }
} catch {
  Write-Host $_.Exception.Message
  Write-Host "Scoop's `coq` package has unexpected format: $scoop_coq"
  exit 1
}
# If environment variable COQ_URL is defined, we check if it's the same
# as the installer being used in the current package in Scoop.
if ($ENV:COQ_URL -ne $null) {
  if ($coq_installer_url -ne $ENV:COQ_URL) {
    Write-Host "ERROR: Latest Scoop Coq version differs from system's Coq version"
    Write-Host "Installed URL: (Environment variable: COQ_URL): $COQ_URL"
    Write-Host "Latest URL: $coq_installer_url"
    exit 1
  }
}
if ($coq_installer_url -match "version.(\d+\.\d+)") {
  # Finally, we fetch the installer's Coq version
  $SCOOP_COQ_VERSION = $matches[1]
  Write-Host "Scoop's coq version: $SCOOP_COQ_VERSION"
}

function Which {
  $cmd = $args[0]
  $Out = Get-Command $cmd -ErrorAction SilentlyContinue
  if ($Out) {
    $Out
  } else {
    Get-Command "$cmd.exe" -ErrorAction SilentlyContinue
  }
}

$CoqDep = Which "coqdep"
$coqc = "coqc"
if ($env:COQC -ne $null) {
    $coqc = $env:COQC
}
Write-Host "System's `coqc`: $coqc"
Write-Host "---------------------------- run: $coqc --version ----------------------------"
# Log the coqc version we used
Invoke-Expression "$coqc --version"
Write-Host "---------------------------- end: $coqc --version ----------------------------"
$zip = "7z"
if ($env:ZIP -ne $null) {
    $zip = $env:ZIP
}

function Get-UnixPath {
  [IO.Path]::Combine($args[0].split("/"))
}

function Parse-Deps {
  $result = @{}
  foreach ($line in Get-Content $args[0]) {
    if ($line -Match "->") {
      $line = $line -Split "->" | ConvertFrom-Json
      $in_file = Get-UnixPath $line[0]
      $out_file = Get-UnixPath $line[1]
      if (-not ($result.ContainsKey($out_file))) {
        $result[$out_file] = New-Object Collections.Generic.List[String]
      }
      $result[$out_file].Add($in_file)
    }
  }
  $result
}


function Parse-Proj {
  # Load a _CoqProject file
  $proj_file = $args[0]
  $Files = [ordered]@{}
  $i = 0;
  foreach($line in Get-Content $proj_file) {
    if ($i -eq 0) {
      $Cmds = $line
    } else {
      $tmp = $i - 1
      # In CoqProject we must
      $line = Get-UnixPath $line
      $Files.Add("$tmp",  $line)
    }
    $i = $i + 1
  }
  @{Files=$Files; Args=$Cmds}
}

function Get-OldExt {
  $file = $args[0]
  $ext = $args[1]
  Join-Path -Path $file.Directory -ChildPath "$($file.BaseName)$ext"
}

function Copy-Ext {
  $file = $args[0]
  $target = $args[1]
  $ext = $args[2]
  $src = Join-Path -Path $file.Directory -ChildPath "$($file.BaseName)$ext"
  $dst = Join-Path -Path $target -ChildPath "$($file.BaseName)$ext"
  Write-Host "copy $src $dst"
  Copy-Item $src $dst
}

function Infer-Deps {
  if ($CoqDep) {
    # We do have coqdep, so we can run it
    $deps_file = Join-Path -Path $build_dir -ChildPath "deps.dot"
    # Generate the dependencies
    Invoke-Expression "$CoqDep -f _CoqProject -dumpgraph $deps_file" | Out-Null
    Parse-Deps $deps_file
  } else {
    # No coqdep installed
    # We should use the order of _CoqProject instead
    Write-Host "$CoqDep not found! No dependencies generated"
    @{}
  }
}

function Build-Proj {
  ################################################################################
  Write-Host "-------------------------------------------------------"
  Write-Host "Building project..."
  Write-Host "-------------------------------------------------------"

  $Proj = Parse-Proj .\_CoqProject

  # Create a _build/Turing directory
  $build_dir = "_build"
  # Build target directory
  $target_dir = Join-Path -Path $build_dir -ChildPath "Turing"
  if (-not ( Test-Path -Path $target_dir -PathType Container )) {
    New-Item -Path $target_dir -ItemType Directory -Force
  }
  Write-Host "Compiling project"
  foreach($line in $Proj.Files.values) {
    $Command = "$coqc $($Proj.Args) $line"
    Write-Host $Command
    # Compile each file
    Invoke-Expression $Command
    $v_file = Get-Item $line
    Copy-Ext $v_file $target_dir ".v"
    Copy-Ext $v_file $target_dir ".vo"
    Copy-Ext $v_file $target_dir ".glob"
  }
  $out_file = Join-Path -Path (Resolve-Path $build_dir) -ChildPath "turing.zip"
  $out_json = Join-Path -Path (Resolve-Path $build_dir) -ChildPath "turing.json"
  Write-Host "-------------------------------------------------------"
  Write-Host "Packaging project..."
  Write-Host "-------------------------------------------------------"
  pushd $build_dir
    # Following, we:
    # 1. create an archive with the compiled binaries
    # 2. generate a Scoop installation script (encoded as a JSON file)
    $Command = "$zip a -r -bb -tzip $out_file Turing"
    # Print out the files being archived
    dir Turing
    # Print out the comment about to be executed (zip ...)
    Write-Host $Command
    # Build the archive
    Invoke-Expression $Command
    # Calculate the hash of the archive
    $h = Get-FileHash $out_file -Algorithm SHA256
    # Create a JSON object that describes the archive (required by Scoop)
    $j = '
    {
        "homepage": "https://gitlab.com/cogumbreiro/turing/",
        "description": "Foundations of Theory of Computation formalized in Coq.",
        "license": "MIT",
        "version": "XXX",
        "url": "XXX",
        "hash": "XXX",
        "depends": "XXX",
        "installer": {
          "script": "Copy-Item \"$dir\\Turing\" \"$(appdir coq $global)\\current\\lib\\coq\\user-contrib\" -Force -Recurse"
        },
        "uninstaller": {
          "script": "Remove-Item \"$(appdir coq $global)\\current\\lib\\coq\\user-contrib\\Turing\""
        }
    }
    ' | ConvertFrom-Json
    # Sets the package version
    $j.version = $VERSION
    # Set the computed hash
    $j.hash = $h.Hash.ToLower()
    # Set the URL of the CI build
    $j.url = "$CI_URL#/dl.7z"
    # Set the dependency
    $j.depends = "coq@$SCOOP_COQ_DEP"
    # Generate the JSON as a string and save it to `turing.json`
    $j | ConvertTo-Json | Tee-Object turing.json
  popd
}

function Generate-Dockerfile {
  Write-Host "Updating docker/Dockerfile with latest Scoop coq package."
  $dockerfile = @"
FROM ubuntu:22.04

RUN apt-get update && \
    apt-get install --yes \
        wine \
        7zip \
        wget \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install PowerShell
ARG PWSH_VERSION=7.4.5
ARG PWSH_URL=https://github.com/PowerShell/PowerShell/releases/download/v`${PWSH_VERSION}/powershell-lts_`${PWSH_VERSION}-1.deb_amd64.deb
RUN wget `$PWSH_URL -O /tmp/powershell.deb && \
    dpkg -i /tmp/powershell.deb && \
    rm -f /tmp/powershell.deb

# Install Coq
ARG COQ_VERSION=$($SCOOP_COQ_DEP)
ARG COQ_URL=$($coq_installer_url)
RUN \
    mkdir /opt/coq && \
    cd /opt/coq && \
    wget `$COQ_URL -O coq.7z && \
    7zz x coq.7z && \
    rm coq.7z

# Make sure wine is not complaining at us
ENV WINEDEBUG=-all
# Make the coq version and URL available as environment variables
ENV COQ_VERSION=`$COQ_VERSION
ENV COQ_URL=`$COQ_URL
# Add a default user
RUN useradd -m builder
USER builder
WORKDIR /home/builder
"@
  $dockerfile | Out-File "docker/Dockerfile"
}

if ($args.Count -ne 1) {
    Write-Host "Usage: build.ps1 <build|docker>"
    exit 1
}
switch ($args[0]) {
    "pkg" {
        Build-Proj
    }
    "docker" {
        Generate-Dockerfile
    }
    default {
        Write-Host "Usage: build.ps1 <pkg|docker>"
        exit 1
    }
}
